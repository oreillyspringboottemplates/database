-- Check Example2 Status
Select * from example2;

-- Using AND/OR in a Where Clause
-- The logical and/or statements are used in 
-- conjunction with the WHERE clause.
SELECT * 
  FROM example2 
 WHERE valid = false  
   AND id = 4;

-- This wouldn't return anything. You would get an 
-- empty set as there are no rows where the value of 
-- valid is F and the value of ID is 4;

-- Try this:   
SELECT * 
  FROM example2 
 WHERE name = 'Frank' 
    OR valid = true;

-- This will return all rows where one or the other 
-- condition is true.


-- ------------------------------------------------------
-- Conditionals
-- ------------------------------------------------------
-- There are plenty of conditionals that can be used to 
-- qualify what results are returned or updated. 
-- Here are examples:
SELECT * 
  FROM example2 
 WHERE CREATED_DATE < '2017-01-01';
 
-- ALWAYS use "IS NULL" or "IS NOT NULL" 
SELECT * 
  FROM example2 
 WHERE NAME IS NOT NULL;

-- This is supported by some DB engines.
-- It is safer to use  "ID <> 4"
SELECT * 
  FROM example2 
 WHERE ID <> 4;           
-- (MySQL supports !=)                                               

   

-- ------------------------------------------------------
-- SYSTEM TABLES 
-- ------------------------------------------------------

-- GET A LIST OF USER TABLES (THAT ARE NOT SYSTEM TABLES)
-- Sometimes, especially working in a console, we want to 
-- know what tables are created in our schema.
-- GET A LIST OF USER TABLES
  SELECT table_schema as database_name, table_name
    FROM information_schema.tables
   WHERE table_type = 'BASE TABLE'
     AND table_schema NOT IN ('information_schema','mysql', 
                                'performance_schema','sys')
ORDER BY database_name, table_name;
 -- make note of the "schemaid"
 

-- GET A LIST OF USER TABLES BY SCHEMA ID
  SELECT table_schema as database_name, table_name
    FROM information_schema.tables
   WHERE table_schema = 'week7';

  
   
-- --------------------------------------------------
-- REMINDER: Inline vs Multi-line SQL 
-- --------------------------------------------------  
-- primary key is written inline

DROP TABLE IF EXISTS example3;   
DROP TABLE IF EXISTS child;
DROP TABLE IF EXISTS parent;


-- You can create Primary Keys in-line in your SQL 
-- (this way is fine [your laptop will smoke])
CREATE TABLE example3(
  id INTEGER PRIMARY KEY 
);

-- You can create Foreign Keys in-line in your SQL 
-- (not recommended in production)
-- drop the tables so we can re-create them
DROP TABLE IF EXISTS parent;
DROP TABLE IF EXISTS child;

CREATE TABLE parent (
  parent_guid INTEGER PRIMARY KEY,
  name VARCHAR(64)
);

CREATE TABLE child (
  child_guid INTEGER PRIMARY KEY,
  child_id INTEGER, 
  CONSTRAINT child_parent_fk FOREIGN KEY (child_guid) 
      REFERENCES parent(parent_guid)
);
-- notice the words "FOREIGN KEY" and the reference 
-- to the local column is not in context since we are 
-- working directly with the column.

-- Can we reference the local table in the Constraint?
--      FOREIGN KEY child(child_guid)

-- You can make this work
-- However, given the complexities of schemas and working 
-- on large projects, it makes more sense to create the foreign 
-- keys in an alter statement. This provides more control over 
-- the constraint. Think about scripts tied to a project. 
-- A developer starts up the project and scripts create a 
-- schema including tables, relations, and dummy data. Separating 
-- functions allows for better script management and execution.


-- drop the tables so we can re-create them
-- let's add the foreign key AFTER creating the table
DROP TABLE IF EXISTS child;
DROP TABLE IF EXISTS parent;

CREATE TABLE parent (
  parent_guid INTEGER PRIMARY KEY,
  name VARCHAR(64)
);

-- FK - Build the table without a FK
CREATE TABLE child (
  child_guid INTEGER,
  child_id INTEGER,
  PRIMARY KEY(child_guid)
);

-- Add FK "after-the-fact"
ALTER TABLE child
ADD CONSTRAINT child_fk 
FOREIGN KEY(child_id) REFERENCES parent(parent_guid);



-- There are some notes (below), but it is better to 
-- review the Lecture notes instead:
-- * Execution Order vs Syntax Order
-- * Indexes
-- * CRUD


-- ------------------------------------------------------
-- EXECUTION ORDER 
-- ------------------------------------------------------ 
-- The SQL order of execution defines the order in which the 
-- clauses of a query are evaluated. Understanding query order 
-- can help you diagnose why a query won't run, and even more 
-- frequently will help you optimize your queries to run faster.
-- 1. from       - selected tables and tables that are "joined" 
--                                                   (join on...)
-- 2. where      - filters the results
-- 3. group by   - aggregates the base data
-- 4. having     - filters the aggregated data
-- 5. select     - returns final data
-- 6. order by   - sorts the final data
-- 7. limit      - limits the size of the result set

 
-- ------------------------------------------------------
-- SYNTAX ORDER 
-- ------------------------------------------------------ 
-- When building a query, make sure your syntax follows the 
-- proper order:

-- 1.	SELECT *
-- 2.	  FROM mytable
-- 	      JOIN another_table
--          ON mytable.column = another_table.column
-- 3.	 WHERE constraint_expression
-- 4. GROUP BY column
-- 5.   HAVING constraint_expression
-- 6. ORDER BY column ASC/DESC
-- 7.    LIMIT count OFFSET COUNT;

-- --------------------------------------------------
-- INDEXES
-- -------------------------------------------------- 
-- Reference Lecture Notes


-- --------------------------------------------------
-- QUERYING
-- -------------------------------------------------- 

-- just in case it already exists
DROP TABLE IF EXISTS example4;

-- We want to query users from an example table. Let's build one
CREATE TABLE example4 (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(64),
    last_name VARCHAR(64),
    email_address VARCHAR(128)
);


-- The large majority of all SQL queries will be select statements. 
-- A select statement access rows of data from a table.
SELECT *
   FROM example4;

-- This simply selects all rows from the table.
-- The asterisk is simply a wildcard that allows us to select all 
-- columns from the table or tables being referenced.

   
-- We could add a WHERE clause in order to select information 
-- given a certain condition.
SELECT *
  FROM example4
 WHERE first_name = 'Jeffery';
-- 0 records found (no inserts yet)
 
-- --------------------------------------------------
-- INSERT
-- -------------------------------------------------- 
-- An INSERT statement allows us to add a new record to a 
-- specified table. It is good form to include the referenced 
-- columns in the INSERT statement.
INSERT INTO example4 (first_name, last_name, email_address)
VALUES ('Jeffery', 'B', 'jbrannon5@oreillyauto.com');   

INSERT INTO example4 (first_name, last_name, email_address)
VALUES ('Sue', 'Smith', 'ssmith9@oreillyauto.com');   

-- In the example above, the auto-generated ID column is not 
-- referenced:
--    * By default, the auto-incremented value is handled by the 
--      database engine
--    * In some databases, like MySQL, you can include 'id' in 
--      the referenced columns and simply add null as the value. 


-- --------------------------------------------------
-- UPDATING
-- -------------------------------------------------- 
-- In some cases, we may need to update data for a particular row.
-- We can do this with an UPDATE statement
UPDATE example4
   SET last_name = 'Smithe' 
 WHERE first_name = 'Sue';
 
-- * Notice that the SET takes a column name and a NEW value 
-- * Also notice that we included a WHERE clause in the update.
-- * It is VERY important to build update statements to reflect exactly 
--   what you want to do.
-- * In the statement above, ALL users with the first name 'Sue' 
--   will be given the last name 'Smithe'
-- * This could be potentially very dangerous since maybe you only 
--   want to update one particular person.

-- Let's query for Sue's ID
SELECT *
  FROM example4
 WHERE first_name = 'SUE';  
-- How many records found? It depends on the collation of the 
-- database and if it is case sensitive.

-- The query above produced records. Maybe we should try for 
-- 'Sue' (not all caps) and see if it still works.
SELECT *
  FROM example4
WHERE first_name = 'Sue';  
-- 1 record found
-- aha! Case Insensitive!

-- One records found with an ID of ?. Now we can build an update 
-- statement to reflect the Globally Unique ID (GUID).
-- NOTE: REPLACE THE QUESTION MARK BELOW WITH THE ID FROM 
--       THE PREVIOUS QUERY
UPDATE example4
   SET last_name = 'Smithers'
 WHERE id = 2;   
-- 1 row updated

-- We updated one, and only one row. This was a successful update.
 SELECT * 
   FROM example4;


-- --------------------------------------------------
-- AND / OR
-- -------------------------------------------------- 

-- Sometime our WHERE clause needs to include more than 
-- one condition. We can add conditions with the reserved word 
-- AND and also with the reserved word "OR"
SELECT *
  FROM example4
 WHERE first_name = 'Jeffery'
   AND  last_name = 'B';    
-- 1 record found

-- The statement above includes two conditions and 1 row was returned
-- We can also use an OR in the clause
SELECT *
  FROM example4
 WHERE first_name = 'Jeffery'
    OR first_name = 'Sue';    
-- 2 records found

-- We can also use this syntax (AND) with an update statement
UPDATE example4
   SET last_name = 'Smith' 
 WHERE first_name = 'Sue'
   AND last_name = 'Smithers';   
-- 1 row updated

-- Check the data (Sue's last name is now "Smith" instead of "Smithe"
SELECT *
  FROM example4;


-- --------------------------------------------------
-- CONDITIONALS
-- -------------------------------------------------- 

 DROP TABLE IF EXISTS oemployees;
 
-- Sometimes we need to check a condition beyond just 
-- checking a string

-- Add a new table and generate some records
CREATE TABLE oemployees (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(30) NOT NULL, 
  last_name VARCHAR(30) NOT NULL,
  hire_date DATE,
  age INTEGER
);


INSERT INTO oemployees (first_name, last_name, hire_date, age)
VALUES 
 ('J', 'B', '2018-01-01', 42)
,('S','S', '2018-02-01', 30);


-- Now we can build a conditional based on factors other than strings
SELECT *
  FROM oemployees
 WHERE hire_date < '2018-02-01';
-- 1 record found

-- The conditional above is based on Date
-- Since 'J' was hired prior to '2018-02-01' only one record is returned
-- Let's try a conditional based on Integer
SELECT *
  FROM oemployees
WHERE age >= 30;   
-- 2 records found

-- The conditional above checks for ages greater than or equal to 30.
-- Since J and S have an age greater than or equal to 30, both records are returned.
-- We can try a conditional based on NULL.
-- Age is not a required field so it is possible for some records not to contain an age.
SELECT *
  FROM oemployees
 WHERE age IS NULL;   
-- No records found

-- Let's add a record with a null value for age:
INSERT INTO oemployees (first_name, last_name, hire_date, age)
VALUES ('B', 'B','2018-01-01', null);

-- Now let's query for NULL
SELECT *
  FROM oemployees
 WHERE age = NULL;
-- Error: Syntax error: Encountered "NULL" at line 3, column 13.
-- Remember we use "IS" and "IS NOT" when we check for NULL

-- Oops! We ran into a syntax error. We checked for 'age = NULL' 
-- instead of 'age IS NULL'
-- Make sure to always check for '<column> IS NULL'
SELECT *
  FROM oemployees
WHERE age IS NULL;
-- 1 record found (much better)


-- We can perform a conditional using the 'not equal' syntax 
-- expressed with '!='
SELECT *
  FROM oemployees
 WHERE age != 42
    OR age IS NULL;
-- MySQL supports bangs equals

-- What happens if we query for only 'age != 42'?
-- Do we see the record with age = null?
SELECT *
  FROM oemployees
 WHERE age != 42;
 
 
 
-- --------------------------------------------------
-- CAPITALIZATION AND SPECIAL CHARACTERS
-- -------------------------------------------------- 
-- Double quotation marks delimit special identifiers
-- This is database-centric. MySQL, for example, can have back 
-- tick (`) or double quotes (") wrap table names and other references.

-- delete table books incase it exists (may fail)
DROP TABLE IF EXISTS yearly; 
DROP TABLE IF EXISTS bot;
DROP TABLE IF EXISTS books;

-- Create a yearly table
CREATE TABLE yearly (
	`year` INTEGER
);

-- ---------------------
-- SINGLE QUOTE EXAMPLE
-- ---------------------
-- A double quotation mark does not need an escape character. 
CREATE TABLE bot (
	greeting VARCHAR(128) DEFAULT '"Hello World!"'
);

CREATE TABLE books (
    title VARCHAR(256)
);

INSERT INTO books (title) 
VALUES ('What''s Eating Gilbert Grape');
-- Notice the single quote is escaped by another single quote

-- Check out the title
SELECT *
  FROM books;


-- ---------------------
-- JAVA EXAMPLE
-- ---------------------

-- To represent a double quotation mark, simply use a double 
-- quotation mark. However, note that in a Java program, a 
-- double quotation mark requires the backslash escape character.

-- e.g. String sql = "SELECT * "; 
--             sql+= "  FROM books ";
--             sql+= " WHERE title = \"What's eating Gilbert Grape\"";



 

-- ---------------------
-- CASE SENSITIVITY
-- ---------------------  
-- SQL keywords are case-insensitive (SELECT or select or SeLeCt)
-- % is a character wildcard when used within character strings 
-- following a LIKE operator
SELECT *
  FROM oemployees
 WHERE first_name LIKE 'J%';   
-- 1 record returned

SELECT *
  FROM oemployees
 WHERE first_name LIKE 'j%';   
-- ? record returned
-- How many records are returned? Why?


 
-- --------------------------------------------------
-- SUBSELECT (SUBQUERY)
-- --------------------------------------------------
 
-- You can obtain a value for an INSERT statement by 
-- SELECTing a value with another statement.
-- This is possible:
INSERT INTO oemployees (first_name, last_name, hire_date, age)
VALUES ('K', 'K', '2018-03-01', 
           (
               SELECT o2.age
                 FROM oemployees o2
                WHERE o2.id = 3
           )
       );

-- In this example we are creating a new record and inserting the 
-- age of the user with id=3

-- Check to see if age is null
SELECT age
  FROM oemployees
 WHERE first_name = 'K';
-- yup!
  

 
-- --------------------------------------------------
-- ALIAS
-- --------------------------------------------------
 
-- We can add an alias for a table in our SQL (with "AS")
SELECT o.* 
  FROM oemployees AS o;
-- having the alias makes it easier to reference tables
-- especially long table names and especially when we 
-- are adding a lot of join statements

-- OR
  
-- We can also add an alias for a table in our SQL without "AS"
SELECT o.* 
  FROM oemployees o;


  
-- When you create an alias that contains a space, in MySQL, 
-- you can wrap the alias in backticks.
SELECT first_name AS `First Name`
  FROM oemployees;



-- When creating an alias, in MySQL, the keyword AS is optional.
SELECT first_name "First Name"
  FROM oemployees;



