
DROP TABLE IF EXISTS example2;

CREATE TABLE example2 (
    id INTEGER,
    name VARCHAR(50) NOT NULL,
    created_date DATE,
    PRIMARY KEY(id)
);

ALTER TABLE example2
ADD COLUMN valid TINYINT;

INSERT INTO example2 (id, name, created_date, valid)
VALUES(1,'Chris','2015-09-22', true);
INSERT INTO example2 (id, name, created_date, valid)
VALUES(2,'Bob','2016-11-11', true);
INSERT INTO example2 (id, name, created_date, valid)
VALUES(3,'Frank','2016-12-12', true);
INSERT INTO example2 (id, name, created_date, valid)
VALUES(4,'Dan','2017-01-13', false);
INSERT INTO example2 (id, name, created_date, valid)
VALUES(5,'Joey','2017-01-24', true);


UPDATE example2 
   SET valid = true;
