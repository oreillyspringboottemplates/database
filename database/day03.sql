 
-- --------------------------------------------------
-- SUM, LIMIT, COUNT, IN, ORDER BY
-- --------------------------------------------------

-- ---------------------
-- SUM
-- --------------------- 
-- Sometimes we need to get the sum for a column given a result set

-- drop table someExample in case it exists (may fail)
DROP TABLE IF EXISTS someExample;
  
CREATE TABLE someExample(
    kount INTEGER NOT NULL
);

INSERT INTO someExample (kount)
VALUES (1), (2), (3), (4), (5);

-- GET THE SUM OF ALL ROWS
-- WHERE KOUNT < 5
SELECT SUM(kount)
  FROM someExample
 WHERE kount < 5;   
-- Result: 10

  
-- ---------------------
-- LIMIT
-- --------------------- 
-- There may be cases where we want to obtain a resultset but we 
-- do not need the full resultset. This happens a lot with pagination. 
-- With pagination, we may only want to display 25 records at a time, 
-- for example. MySQL, MS SQL, Oracle�
SELECT * 
  FROM someExample
 LIMIT 3;


   
-- ---------------------
-- COUNT
-- --------------------- 

-- Count will return the count of records that occur based on 
-- your query qualifiers.
 SELECT COUNT(*) AS "COUNT"
   FROM someExample
  WHERE kount < 4;
-- Result: 3

  
-- ---------------------
-- IN
-- --------------------- 

-- In allows you to qualify that something is in a collection.
SELECT *
  FROM someExample
 WHERE kount IN (1,2,3,4,5);
-- returns all rows
  
  
-- ---------------------
-- ORDER BY
-- --------------------- 
-- Order by allows you to order the way your result set is returned. 
-- This is useful for sorting data for display. 
-- You can order by multiple columns. 
-- You can order results ascending (ASC) or descending (DESC)

-- ASCENDING
  SELECT *
    FROM someExample
   WHERE kount IN (1,2,3,4,5)
ORDER BY kount ASC;

-- DESCENDING
  SELECT *
    FROM someExample
   WHERE kount IN (1,2,3,4,5)
ORDER BY kount DESC; 
 
-- What happens when �ASC� and �DESC� are both missing from the 
-- ORDER BY clause???
  SELECT *
    FROM someExample
   WHERE kount IN (1,2,3,4,5)
ORDER BY kount;
 
  
-- --------------------------------------------------
-- UNION
-- --------------------------------------------------

-- The SQL union statement is used to combine two select statements 
-- and remove the duplicate results between the combined result sets.

-- drop tables in case they exist
DROP TABLE IF EXISTS tableOne;
DROP TABLE IF EXISTS tableTwo;

-- Create two tables
CREATE TABLE tableOne(
    id INTEGER NOT NULL,
    zip VARCHAR(5) NOT NULL
);

CREATE TABLE tableTwo(
    date DATE NOT NULL,
    id INTEGER NOT NULL,
    zip VARCHAR(5) NOT NULL
);

-- Populate tables
INSERT INTO tableOne (id, zip) 
VALUES (12, '63028'), (13, '65804');

INSERT INTO tableTwo (date, id, zip) VALUES 
 ('2015-12-12', 1, '65804')
,('2017-11-10', 2, '63038');


-- UNION EXAMPLE!
SELECT zip 
  FROM tableOne 
 UNION 
SELECT zip 
  FROM tableTwo;
-- Returns 3 rows (only 1 of 2 rows from tableOne and 2 rows from tableTwo)
-- 63028
-- 63038
-- 65804
  
-- Notice the duplicate value 65804 is only displayed once. 
-- If you wanted to return duplicates you could use UNION ALL statement.
   SELECT zip  
     FROM tableOne 
UNION ALL 
   SELECT zip  
     FROM tableTwo;
-- now we see ALL rows!
-- 63028
-- 65804
-- 65804
-- 63038
     
-- UNION WITH 'HARD CODED' DATA:
-- * Union's do not have to be done on the same named column:
--    ** However, the data type of the select statements that are 
--       being 'union-ed' must be the same. 
--    ** Unions may also be on multiple selected columns as long as 
--       the column # is the same between the two select statements
--       and the data types correspond.
   SELECT id, zip, '1970-01-01' AS date
     FROM tableOne 
UNION ALL
   SELECT ID, ZIP, DATE
     FROM tableTwo;


-- UNION WITH DIFFERENT COLUMN NAMES:
-- drop tables: employees / employee_benefits
DROP TABLE IF EXISTS employees;
DROP TABLE IF EXISTS employee_benefits;

CREATE TABLE employees (
    employee_id INTEGER NOT NULL PRIMARY KEY,
    start_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    employee_name VARCHAR(64) NOT NULL
);


CREATE TABLE employee_benefits (
    benefit_id INTEGER NOT NULL PRIMARY KEY,
    employee_name VARCHAR(64) NOT NULL,
    anniversary_date TIMESTAMP NOT NULL,
    benefit_type VARCHAR(4)
);


-- populate employees table
INSERT INTO employees (employee_id, start_date, employee_name)
VALUES (234, '2017-12-11', 'jack baur')
,(244, '2014-06-09', 'nick fornandez')
,(2222, '2012-03-04', 'cory spelt');

-- populate employee_benefits table
INSERT INTO employee_benefits (benefit_id, employee_name, 
            anniversary_date, benefit_type)
VALUES (245, 'don bluth', '2017-04-12', 'FULL')
,(555, 'dick tracy', '2011-06-06', 'PART');

-- Execute UNION with different column names
SELECT start_date, "name" 
  FROM employees 
 UNION 
SELECT anniversary_date, employee_name 
  FROM employee_benefits;
-- The above sql statement will work correctly despite selecting 
-- differently named columns!
-- Notice how the columns are labeled (1, 2)

-- We can "fix" the column names by using an alias
SELECT start_date AS "DATE", '' AS "NAME"
  FROM employees 
 UNION 
SELECT anniversary_date AS "DATE", employee_name AS "NAME"
  FROM employee_benefits; 
     
  
-- --------------------------------------------------
-- SUB QUERIES
-- --------------------------------------------------
-- Sub queries, also known as inner queries, are used to perform 
-- multi part or step sequel operations.
INSERT INTO employee_benefits (benefit_id, employee_name, 
            anniversary_date, benefit_type)
VALUES (234, 'jack baur', '2018-12-11', 'FULL');

-- SUB-SELECT
SELECT e.employee_name,
       e.employee_id, 
       e.start_date,
       (
          SELECT eb.anniversary_date
            FROM employee_benefits eb
           WHERE eb.benefit_id = e.employee_id
       ) AS anniversary_date 
  FROM employees e;

-- ALTERNATIVELY � USE A JOIN (makes more sense)
   SELECT e.employee_name,
          e.employee_id, 
          e.start_date,
          eb.anniversary_date
     FROM employees e
LEFT JOIN employee_benefits eb
       ON e.employee_id = eb.benefit_id;

    
  
  
-- --------------------------------------------------
-- JOIN
-- --------------------------------------------------

-- Joins are used to take two tables of data and combine them 
-- together based on relationships between the two tables 
-- (and sometimes tables with no relation).

  
-- --------------------------------------------------
-- INNER JOIN
-- --------------------------------------------------

-- Inner join takes two tables with a common column and then 
-- joins the table based on the common columns or intersection 
-- and returns results based on what exists in both tables.

-- delete tables in case they exist
DROP TABLE IF EXISTS a;
DROP TABLE IF EXISTS b;
    
-- Create table a and table b
CREATE TABLE a (
    id INTEGER NOT NULL,
    b_id INTEGER NOT NULL
);	

CREATE TABLE b (
    id INTEGER NOT NULL,
    word VARCHAR(12),
    valid VARCHAR(1)
);

-- Add Data to table a
INSERT INTO a (id, b_id)
VALUES (1,5), (2,2), (3,9), (4,4), (5,2);	

-- Add Data to table b
INSERT INTO b (id, word, valid)
VALUES (1,'test','T'), (2,'fun','F'), (3,'stuff','T'), 
       (4,'alright','T'), (6,'super','T'), (7,'awesome','F'), 
       (8,'great','F'), (9,'math','F');

-- Create a SQL script to perform an inner join from a on b 
-- where IDs match and valid = 'T'
    SELECT a.*, b.* 
      FROM a 
INNER JOIN b 
        ON a.b_id = b.id;


-- Like all queries this can be further qualified. (Add a clause)
    SELECT * 
      FROM a 
INNER JOIN b
        ON a.b_id = b.id 
     WHERE b.valid = 'T';
        
        
        
-- --------------------------------------------------
-- OUTER JOIN
-- --------------------------------------------------  
-- Outer join takes two tables then joins the table based on some 
-- common columns or intersection and returns the full result set 
-- of one or both tables along with the common intersection between 
-- the two.
  
  

-- --------------------------------------------------
-- LEFT OUTER JOIN
-- -------------------------------------------------- 
         SELECT * 
           FROM a
LEFT OUTER JOIN b
             ON a.b_id = b.id;
-- * Notice the first record has null's in the columns for B. 
-- * B does not have a matching entry with A, but we are doing a 
--   left outer join so all the records from A are returned regardless. 
-- * Thus the columns from B are given the values null for the first 
--   record as they do not exist.



-- --------------------------------------------------
-- RIGHT OUTER JOIN
-- -------------------------------------------------- 
          SELECT * 
            FROM a
RIGHT OUTER JOIN b
              ON a.b_id = b.id;


-- --------------------------------------------------
-- FULL OUTER JOIN
-- -------------------------------------------------- 
         SELECT * 
           FROM a
FULL OUTER JOIN b
             ON a.b_id = b.id;
-- Apache Derby does not support the query above; it fails


-- --------------------------------------------------
-- LEFT EXCLUDING JOIN
-- -------------------------------------------------- 
-- This query will return all of the records in the left table (table A) 
-- that do not match any records in the right table (table B).
   SELECT *
     FROM a
LEFT JOIN b
       ON a.b_id = b.id
    WHERE b.id IS NULL;

-- --------------------------------------------------
-- RIGHT EXCLUDING JOIN
-- -------------------------------------------------- 
-- This query will return all of the records in the left table (table A) 
-- that do not match any records in the right table (table B).
    SELECT *
      FROM a
RIGHT JOIN b
        ON a.b_id = b.id
     WHERE a.b_id IS NULL;

   
-- --------------------------------------------------
-- OUTER EXCLUDING JOIN
-- -------------------------------------------------- 
-- This query will return all of the records in the left table (table A) and all of the records in the right table (table B) that do not match. 
-- You may never actually need this�
   SELECT * 
     FROM 
          (
                 SELECT a.id AS a_id, b_id, b.word, b.valid, 
                        -1 as b_id2 
                   FROM a
              LEFT JOIN b 
                     ON a.b_id = b.id
                  WHERE b.id IS NULL
                  UNION
                 SELECT a.id AS a_id, b_id, b.word, b.valid,
                        b.id as b_id2
                   FROM a
             RIGHT JOIN b 
                     ON a.b_id = b.id
                  WHERE (a.b_id IS NULL OR b.id IS NULL)
          ) x
ORDER BY x.b_id2;


    
-- --------------------------------------------------
-- EXPLAIN PLAN (CALLED RUNTIMESTATISTICS IN DERBY)
-- --------------------------------------------------

-- Build table one (intern table)
CREATE TABLE interns(
	team_member_number INTEGER,
	hacker_rank_id INTEGER,
	first_name VARCHAR(64),
	last_name VARCHAR(64),
	PRIMARY KEY(team_member_number)
);

CREATE INDEX hackerrankid_idx ON interns(hacker_rank_id);

-- Build table one (score table)
CREATE TABLE intern_scores(
	hacker_rank_id INTEGER,
	milestone_id INTEGER NOT NULL,
	milestone_score DECIMAL(8,2) NOT NULL,
	PRIMARY KEY(hacker_rank_id),
	CONSTRAINT hackerrank_interns_fk FOREIGN KEY(hacker_rank_id) 
	     REFERENCES interns(hacker_rank_id)
);

-- Insert some test data
INSERT INTO interns(team_member_number, hacker_rank_id, first_name, last_name)
VALUES 
 (4000000, 2546, 'Michael', 'Jackson')
,(4000001, 2546, 'Michael', 'Jackson')
,(4000002, 2546, 'Michael', 'Jackson');


-- turn on RUNTIMESTATISTICS for connection (APACHE DERBY):
CALL SYSCS_UTIL.SYSCS_SET_RUNTIMESTATISTICS(1);


-- execute complex query here -- step through the result set
SELECT *
  FROM interns interns
  JOIN intern_scores scores
    ON scores.hacker_rank_id = interns.hacker_rank_id;
     

-- access runtime statistics information (APACHE DERBY):
VALUES SYSCS_UTIL.SYSCS_GET_RUNTIMESTATISTICS();

-- turn off runtimestatistics (APACHE DERBY)
CALL SYSCS_UTIL.SYSCS_SET_RUNTIMESTATISTICS(0);


-- --------------------------------------------------
-- ATOMIC TRANSACTION (APACHE DERBY)
-- --------------------------------------------------
-- We cannot test this in a SQL window since "autocommit" is not supported
-- We would have to run this in a shell (ij) or in a Web Application with
-- Transaction Management
autocommit off;
CREATE TABLE foo3(
  id INTEGER,
  first_name VARCHAR(64),
  PRIMARY KEY(id)
);

INSERT INTO foo3(id, first_name)
VALUES (1, 'Jim');
-- Succeeds

INSERT INTO foo3(id, first_name)
VALUES (1, 'Jim');
-- FAILS

commit;
autocommit on;

